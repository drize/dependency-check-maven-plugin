package com.maven.plugin.dependency.ckeck;

import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.ArtifactRepositoryPolicy;
import org.apache.maven.artifact.repository.MavenArtifactRepository;
import org.apache.maven.artifact.repository.layout.DefaultRepositoryLayout;
import org.apache.maven.plugin.testing.MojoRule;
import org.apache.maven.plugin.testing.resources.TestResources;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;
import java.nio.file.Paths;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DependencyCheckMojoTest {

    @Rule
    public MojoRule rule = new MojoRule() {


        @Override
        protected void before() throws Throwable {
        }

        @Override
        protected void after() {
            getContainer().getContext().getContextData();
        }
    };

    @Rule
    public TestResources resources = new TestResources();

    /**
     * Generate a local repository
     * @return local repository object
     */
    private ArtifactRepository createLocalArtifactRepository() {
        File localRepoDir = new File(System.getProperty("java.io.tmpdir"), "local-repo." + System.currentTimeMillis());
                  localRepoDir.mkdirs();
        return new MavenArtifactRepository("local",
                localRepoDir.toURI().toString(),
                new DefaultRepositoryLayout(),
                new ArtifactRepositoryPolicy( true, ArtifactRepositoryPolicy.UPDATE_POLICY_ALWAYS, ArtifactRepositoryPolicy.CHECKSUM_POLICY_IGNORE ),
                new ArtifactRepositoryPolicy( true, ArtifactRepositoryPolicy.UPDATE_POLICY_ALWAYS, ArtifactRepositoryPolicy.CHECKSUM_POLICY_IGNORE )

        );
    }

    @Test
    public void execute() throws Exception {
//        File pomFile = Paths.get("target/test-classes/project-to-test/").toFile();
//        assertNotNull(pomFile);
//        assertTrue(pomFile.exists());
//        rule.executeMojo(pomFile, "tree");
    }
}