package com.maven.plugin.dependency.ckeck;

import java.io.File;
import java.net.URI;
import java.util.Date;
import java.util.List;

import org.apache.maven.DefaultMaven;
import org.apache.maven.Maven;
import org.apache.maven.artifact.InvalidRepositoryException;
import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.DefaultMavenExecutionResult;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.execution.MavenExecutionRequestPopulator;
import org.apache.maven.execution.MavenExecutionResult;
import org.apache.maven.model.building.ModelBuildingRequest;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;
import org.apache.maven.project.ProjectBuildingRequest;
import org.apache.maven.project.ProjectBuildingResult;
import org.apache.maven.repository.RepositorySystem;
import org.apache.maven.settings.building.SettingsBuilder;
import org.apache.maven.settings.crypto.SettingsDecrypter;
import org.codehaus.plexus.PlexusContainer;
import org.codehaus.plexus.component.repository.exception.ComponentLookupException;
import org.eclipse.aether.impl.RemoteRepositoryManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MavenDependencyReader {

	private final PlexusContainer plexus;
	private final DefaultMaven maven;
	private final ProjectBuilder projectBuilder;
	private final RepositorySystem repositorySystem;
	private final MavenExecutionRequestPopulator populator;
	private final SettingsBuilder settingsBuilder;
	private final RemoteRepositoryManager remoteRepositoryManager;
	private final SettingsDecrypter settingsDecrypter;
	private static final Object lastLocalRepositoryLock = new Object();
	private static URI lastLocalRepository;

	public MavenDependencyReader(PlexusContainer plexusContainer) throws ComponentLookupException {
		plexus = plexusContainer;
		this.maven = (DefaultMaven) plexus.lookup(Maven.class);
		this.projectBuilder = plexus.lookup(ProjectBuilder.class);
		this.repositorySystem = plexus.lookup(RepositorySystem.class);
		this.settingsBuilder = plexus.lookup(SettingsBuilder.class);
		this.populator = plexus.lookup(MavenExecutionRequestPopulator.class);
		this.settingsDecrypter = plexus.lookup(SettingsDecrypter.class);
		this.remoteRepositoryManager = plexus.lookup(RemoteRepositoryManager.class);
	}

	public MavenExecutionResult readProjectWithDependencies(MavenExecutionRequest req) {

		File pomFile = req.getPom();
		MavenExecutionResult result = new DefaultMavenExecutionResult();
		try {
			ProjectBuildingRequest configuration = req.getProjectBuildingRequest();
			configuration.setValidationLevel(ModelBuildingRequest.VALIDATION_LEVEL_MINIMAL);
			configuration.setResolveDependencies(true);
			configuration.setRepositorySession(maven.newRepositorySession(req));
			ProjectBuildingResult projectBuildingResult = projectBuilder.build(pomFile, configuration);
			result.setProject(projectBuildingResult.getProject());
			result.setDependencyResolutionResult(projectBuildingResult.getDependencyResolutionResult());
		}
		catch (ProjectBuildingException ex) {
			//don't add the exception here. this should come out as a build marker, not fill
			//the error logs with msgs
			return result.addException(ex);
		}

		return result;
	}

	public MavenExecutionRequest newMavenRequest(File pom, List<String> activeProfiles, List<String> inactiveProfiles, List<String> goals)
			throws InvalidRepositoryException {
		MavenExecutionRequest request = new DefaultMavenExecutionRequest();
		request.setGoals(goals);
		request.setPom(pom);
		request.setActiveProfiles(activeProfiles);
		request.setLocalRepository(repositorySystem.createDefaultLocalRepository());
		request.setInactiveProfiles(inactiveProfiles);
		request.setStartTime(new Date());
		return request;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("MavenDependencyReader{");
		sb.append("plexus=").append(plexus);
		sb.append(", maven=").append(maven);
		sb.append(", projectBuilder=").append(projectBuilder);
		sb.append(", repositorySystem=").append(repositorySystem);
		sb.append(", populator=").append(populator);
		sb.append(", settingsBuilder=").append(settingsBuilder);
		sb.append(", settingsDecrypter=").append(settingsDecrypter);
		sb.append('}');
		return sb.toString();
	}
}
