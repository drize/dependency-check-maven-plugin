package com.maven.plugin.dependency.ckeck;

import java.nio.file.Paths;
import java.util.Collections;

import org.apache.maven.artifact.InvalidRepositoryException;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.execution.MavenExecutionResult;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.PlexusContainer;
import org.codehaus.plexus.component.repository.exception.ComponentLookupException;

@Mojo(name = "validate")
public class DependencyCheckMojo extends AbstractMojo {

	@Parameter(defaultValue = "${basedir}")
	String basedir;

	@Component
	protected PlexusContainer container;



	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		try {
			MavenDependencyReader mavenDependencyReader = new MavenDependencyReader(container);
			getLog().info("Dependency reader : " + mavenDependencyReader);
			MavenExecutionRequest mavenExecutionRequest = mavenDependencyReader
					.newMavenRequest(Paths.get(basedir, "pom.xml").toFile(), Collections.emptyList(), Collections.emptyList(),
							Collections.emptyList());
			MavenExecutionResult mavenExecutionResult = mavenDependencyReader.readProjectWithDependencies(mavenExecutionRequest);
			getLog().info("Dependencies size :" + mavenExecutionResult.getDependencyResolutionResult().getDependencies().size());

		}
		catch (ComponentLookupException | InvalidRepositoryException e) {
			getLog().info(e);
		}

	}

}
